FROM openjdk:11-jre-slim
LABEL maintainer="Stephane ROBERT"

ENV DEBIAN_FRONTEND noninteractive
ENV ANSIBLE_HOST_KEY_CHECKING false
ENV RDECK_JVM_SETTINGS "-Xmx1024m -Xms256m -XX:MaxMetaspaceSize=256m -server"

COPY files/rundeck-gpg.key files/bintray-gpg.key files/python-requirements.txt  /root/
COPY build/libs/ansible-plugin-*.jar /var/lib/rundeck/libext/
COPY files/rundeck.sh /rundeck.sh

RUN mkdir -p /usr/share/man/man1 \
  && mkdir -p /root/playbooks /root/.ssh \
  && apt-get -y update -y \
	&& apt-get -y upgrade \
  && apt install -y gnupg apt-utils apt-transport-https \
	&& cat /root/*-gpg.key | apt-key add - \
	&& echo "deb http://dl.bintray.com/rundeck/rundeck-deb /" > /etc/apt/sources.list.d/rundeck.list \
  && apt-get -y update -y \
	&& apt-get -y install --no-install-suggests --no-install-recommends --allow-unauthenticated openssh-client  \
		ca-certificates \
		subversion \
    curl \
    uuid-runtime \
    gcc \
    python3.7-dev \
    python3.7-distutils \
    libffi-dev \
    libssl-dev \
    sudo \
		sshpass \
		rundeck \
		default-mysql-client \
	&& apt-get -y clean \
	&& rm -Rf /var/lib/apt/lists/* \
  && curl -sLo /usr/local/bin/ep \
  'https://github.com/kreuzwerker/envplate/releases/download/1.0.0-RC1/ep-linux' \
  && chmod +x /usr/local/bin/ep \
  && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
  && python3.7 get-pip.py \
  && pip install --upgrade pip setuptools setuptools_scm \
  && pip install -r /root/python-requirements.txt

# Keep here unless error on installation of rundeck package
COPY ./files/rundeck-config.properties ./files/admin.aclpolicy ./files/framework.properties ./files/jaas-activedirectory.conf ./files/log4j.properties ./files/realm.properties /etc/rundeck/
#COPY ./files/ssl.properties ./files/keystore ./files/truststore /etc/rundeck/ssl/

RUN chmod +x /rundeck.sh \
  && find /etc/rundeck -type f -exec chmod 644 {} \; \
  && find /etc/rundeck -type d -exec chmod 755 {} \; \
  && chown -R rundeck:rundeck /var/lib/rundeck/

#EXPOSE 4443
EXPOSE 4440
CMD /rundeck.sh
