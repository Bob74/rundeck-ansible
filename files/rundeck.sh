#!/bin/bash

# export RUNDECK_WITH_SSL=true
# export RDECK_HTTPS_PORT=4443

for file in $(ls /etc/rundeck/{framework.properties,log4j.properties,profile,realm.properties,rundeck-config.properties}); do
	/usr/local/bin/ep -v $file
done


. /etc/rundeck/profile

echo $rundeckd

$rundeckd
